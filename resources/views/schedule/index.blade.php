@extends('layouts.app')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Jadwal</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-6"></div>
                <div class="col-3 ml-auto">
                    <div class="d-flex align-items-center justify-content-end mb-3">
                        @if(request()->has('search'))
                            <a href="{{ request()->url() }}" class="btn btn-secondary mr-3">Reset</a>
                        @endif

                        <form action="{{ request()->url() }}">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search by Matakuliah.." aria-label="Search..." aria-describedby="search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit" id="search">
                                        <i class="fas fa-fw fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Jam</th>
                            <th>Nama Dosen</th>
                            <th>Mata Kuliah</th>
                            <th>Status</th>
                            <th>Verifikasi Admin</th>
                            <th style="display: none;">Alasan</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($lectures as $lecture)
                        @php
                            $course = optional($lecture->course);
                            $dosen = optional($course->dosen);
                        @endphp
                        <tr>
                            <td>
                                <span class="text-nowrap">{{ $lecture->date->format('l, d M Y') }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $lecture->time }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $dosen->name }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $course->name }}</span>
                            </td>
                            <td>
                                @if($lecture->status)
                                    @if($lecture->status == 1 || $lecture->status == 3)
                                        <div class="badge badge-success d-block p-2">Masuk</div>
                                    @elseif($lecture->status == -1 || $lecture->status == 4)
                                        <div class="badge badge-danger d-block p-2">Tidak Masuk</div>
                                    @endif
                                @else
                                    <div class="badge badge-info d-block p-2">Belum Ada Respon</div>
                                @endif
                            </td>
                            <td>
                                <div class="text-nowrap text-center">
                                @if($lecture->status < 2)
                                    <form class="d-inline-block" action="{{ url('/schedule/'.$lecture->id.'/update-status') }}" method="POST">
                                        @csrf

                                        <input type="hidden" name="status" value="3">

                                        <button type="submit" class="btn bg-success text-white btn-sm">Mengajar</button>
                                    </form>

                                    <form class="d-inline-block" action="{{ url('/schedule/'.$lecture->id.'/update-status') }}" method="POST">
                                        @csrf

                                        <input type="hidden" name="status" value="4">

                                        <button type="submit" class="btn bg-danger text-white btn-sm">Tidak Mengajar</button>
                                    </form>
                                @else
                                    <a href="" class="btn btn-secondary btn-sm disabled" disabled>Mengajar</a>
                                    <a href="" class="btn btn-secondary btn-sm disabled" disabled>Tidak Mengajar</a>
                                @endif
                                </div>
                            </td>
                            <td style="display: none;">
                                <div class="text-nowrap">
                                    {{ Str::limit($lecture->message, 30) }}
                                    @if(strlen($lecture->message) > 30)
                                        <a href="#" data-toggle="modal" data-target="#reasonModal-{{ $lecture->id }}">
                                            Read More
                                        </a>
                                    @endif

                                    <!-- Modal -->
                                    <div class="modal fade" id="reasonModal-{{ $lecture->id }}" tabindex="-1" role="dialog" aria-labelledby="reasonModal-{{ $lecture->id }}Label" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Alasan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        {{ $lecture->message }}
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            {!! $lectures->links() !!}
        </div>
    </div>
@endsection
