<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div>
                    <!-- Page Heading -->
                <!-- DataTales Example -->
                <div class="card" style="border: none;">
                    <div class="card-body">
                        <h1 class="h3 mb-4 text-gray-800">Jadwal</h1>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                        <th>Nama Dosen</th>
                                        <th>Mata Kuliah</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($lectures as $lecture)
                                    @php
                                        $course = optional($lecture->course);
                                        $dosen = optional($course->dosen);
                                    @endphp
                                    <tr>
                                        <td>
                                            <span class="text-nowrap">{{ $lecture->date->format('l, d M Y') }}</span>
                                        </td>
                                        <td>
                                            <span class="text-nowrap">{{ $lecture->time }}</span>
                                        </td>
                                        <td>
                                            <span class="text-nowrap">{{ $dosen->name }}</span>
                                        </td>
                                        <td>
                                            <span class="text-nowrap">{{ $course->name }}</span>
                                        </td>
                                        <td>
                                            @if($lecture->status)
                                                @if($lecture->status == 1 || $lecture->status == 3)
                                                    <div class="badge badge-success d-block p-2">Masuk</div>
                                                @elseif($lecture->status == -1 || $lecture->status == 4)
                                                    <div class="badge badge-danger d-block p-2">Tidak Masuk</div>
                                                @endif
                                            @else
                                                <div class="badge badge-info d-block p-2">Belum Ada Respon</div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                </div>
                <!-- /.container-fluid -->

            </div>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    @yield('scripts')
</body>
</html>