@extends('layouts.app')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Mahasiswa</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <a href="{{ url('student/create') }}" class="btn btn-primary">Add Mahasiswa</a>
                </div>
                <div class="col-3 ml-auto">
                    <div class="d-flex align-items-center justify-content-end mb-3">
                        @if(request()->has('search'))
                            <a href="{{ request()->url() }}" class="btn btn-secondary mr-3">Reset</a>
                        @endif

                        <form action="{{ request()->url() }}">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search by Name..." aria-label="Search..." aria-describedby="search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit" id="search">
                                        <i class="fas fa-fw fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Chat ID</th>
                        <th>Nomor Handphone</th>
                        <th>Matakuliah</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($students as $student)
                        <tr>
                            <td>
                                <span class="text-nowrap">{{ $student->name }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $student->chat_id }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $student->no_tele }}</span>
                            </td>
                            <td width="220">
                                <div class="text-nowrap">
                                    <span class="text-nowrap">
                                        {{ $student->courses->take(2)->implode('name', ', ') }}
                                        @if(count($student->courses->toArray()) > 2)
                                            ...
                                        @endif

                                        <a href="#" data-toggle="modal" data-target="#coursesModal-{{ $student->id }}">
                                            View Detail
                                        </a>


                                    </span>

                                    <!-- Modal -->
                                    <div class="modal fade" id="coursesModal-{{ $student->id }}" tabindex="-1" role="dialog" aria-labelledby="coursesModal-{{ $student->id }}Label" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Matakuliah</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama</th>
                                                                <th>Hari</th>
                                                                <th>Jam</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($student->courses as $course)
                                                            <tr>
                                                                <td>{{ $course->name }}</td>
                                                                <td>{{ $course->days }}</td>
                                                                <td>{{ $course->time_start }} - {{ $course->time_end }}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="width: 120px;" class="text-center">
                                <a href="{{ url('/student/'.$student->id.'/edit') }}" class="btn btn-info btn-sm">
                                    <i class="fas fa-fw fa-edit"></i>
                                </a>
                                <form class="d-inline-block" action="{{ url('/student/'.$student->id) }}" method="POST">
                                    @csrf
                                    @method('delete')

                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin?')">
                                        <i class="fas fa-fw fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            {!! $students->links() !!}
        </div>
    </div>
@endsection
