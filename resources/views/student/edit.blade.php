@extends('layouts.app')

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Mahasiswa</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body p-5">
            <form action="{{ url('student/'.$student->id) }}" method="POST">
                @csrf
                @method('put')

                <div class="mb-4">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-4">
                            <input
                                class="form-control @error('name') is-invalid @enderror"
                                type="text"
                                name="name"
                                id="inputName"
                                placeholder="Nama"
                                value="{{ $student->name ?? old('name') }}"
                            >
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">No Telegram</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="phone-number">+62</span>
                                </div>
                                <input
                                    class="form-control @error('no_tele') is-invalid @enderror"
                                    type="number"
                                    name="no_tele"
                                    placeholder="No Telegram"
                                    aria-label="No Handphone"
                                    aria-describedby="phone-number"
                                    value="{{ $student->no_tele ?? old('name') }}"
                                >
                                @error('no_tele')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Matakuliah</label>
                        <div class="col-sm-4">
                            <select class="form-control js-select" name="courses[]" multiple="multiple">
                                @foreach(\App\Course::all() as $course)
                                    <option value="{{ $course->id }}" @if(in_array($course->id, $student->courses->pluck('id')->all())) selected @endif>
                                        {{ $course->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 offset-sm-2 text-right">
                        <button type="submit" class="btn btn-primary px-5">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.js-select').select2({
            placeholder: "Pilih Matakuliah"
        });
    });
</script>
@endsection
