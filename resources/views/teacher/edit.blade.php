@extends('layouts.app')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Dosen</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body p-5">
            <form action="{{ url('teacher/'.$teacher->id) }}" method="POST">
                @csrf
                @method('put')

                <div class="mb-4">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-4">
                            <input
                                class="form-control @error('name') is-invalid @enderror"
                                type="text"
                                name="name"
                                id="inputName"
                                placeholder="Nama"
                                value="{{ $teacher->name ?? old('name') }}"
                            >
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">No Telegram</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="phone-number">+62</span>
                                </div>
                                <input
                                    class="form-control @error('no_tele') is-invalid @enderror"
                                    type="number"
                                    name="no_tele"
                                    placeholder="No Telegram"
                                    aria-label="No Handphone"
                                    aria-describedby="phone-number"
                                    value="{{ $teacher->no_tele ?? old('name') }}"
                                >
                                @error('no_tele')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 offset-sm-2 text-right">
                        <button type="submit" class="btn btn-primary px-5">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
