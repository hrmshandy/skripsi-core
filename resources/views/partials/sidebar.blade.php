<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Scheduler</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ request()->is('/') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('schedule') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/schedule') }}">
            <i class="fas fa-fw fa-clock"></i>
            <span>Jadwal</span>
        </a>
    </li>

    <li class="nav-item {{ request()->is('schedule-screen') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/schedule-screen') }}" target="_blank">
            <i class="fas fa-fw fa-clock"></i>
            <span>Screen Jadwal</span>
        </a>
    </li>

    <li class="nav-item {{ request()->is('history') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/history') }}">
            <i class="fas fa-fw fa-book"></i>
            <span>Laporan</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Master Data
    </div>

    <li class="nav-item {{ request()->is('teacher') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/teacher') }}">
            <i class="fas fa-fw fa-chalkboard-teacher"></i>
            <span>Dosen</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('student') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/student') }}">
            <i class="fas fa-fw fa-user-graduate"></i>
            <span>Mahasiswa</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('course') ? 'active' : null }}">
        <a class="nav-link" href="{{ url('/course') }}">
            <i class="fas fa-fw fa-book"></i>
            <span>Matakuliah</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
