@extends('layouts.app')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Matakuliah</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <a href="{{ url('course/create') }}" class="btn btn-primary">Add Matakuliah</a>
                </div>
                <div class="col-3 ml-auto">
                    <div class="d-flex align-items-center justify-content-end mb-3">
                        @if(request()->has('search'))
                            <a href="{{ request()->url() }}" class="btn btn-secondary mr-3">Reset</a>
                        @endif

                        <form action="{{ request()->url() }}">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search by Name..." aria-label="Search..." aria-describedby="search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit" id="search">
                                        <i class="fas fa-fw fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Dosen</th>
                        <th>Hari</th>
                        <th>Jam</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($courses as $course)
                        <tr>
                            <td>
                                <span class="text-nowrap">{{ $course->name }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ optional($course->dosen)->name }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $course->days }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $course->time_start.' - '. $course->time_end }}</span>
                            </td>
                            <td style="width: 120px;" class="text-center">
                                <a href="{{ url('/course/'.$course->id.'/edit') }}" class="btn btn-info btn-sm">
                                    <i class="fas fa-fw fa-edit"></i>
                                </a>
                                <form class="d-inline-block" action="{{ url('/course/'.$course->id) }}" method="POST">
                                    @csrf
                                    @method('delete')

                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin?')">
                                        <i class="fas fa-fw fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            {!! $courses->links() !!}
        </div>
    </div>
@endsection
