@extends('layouts.app')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Matakuliah</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body p-5">
            <form action="{{ url('course/'.$course->id) }}" method="POST">
                @csrf
                @method('put')

                <div class="mb-4">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-4">
                            <input
                                class="form-control @error('name') is-invalid @enderror"
                                type="text"
                                name="name"
                                id="inputName"
                                placeholder="Nama"
                                value="{{ $course->name ?? old('name') }}"
                            >
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputDosen" class="col-sm-2 col-form-label">Dosen</label>
                        <div class="col-sm-4">
                            <select
                                class="form-control @error('id_dosen') is-invalid @enderror"
                                name="id_dosen"
                                id="inputDosen"
                            >
                                <option value="">Pilih Dosen</option>
                                @foreach(\App\Dosen::all() as $dosen)
                                    <option value="{{ $dosen->id }}" @if($course->id_dosen == $dosen->id or old('id_dosen') == $dosen->id) selected @endif>{{ $dosen->name }}</option>
                                @endforeach
                            </select>
                            @error('id_dosen')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputDays" class="col-sm-2 col-form-label">Hari</label>
                        <div class="col-sm-4">
                            <input
                                class="form-control @error('days') is-invalid @enderror"
                                type="text"
                                name="days"
                                id="inputDays"
                                placeholder="Hari"
                                value="{{ $course->days ?? old('days') }}"
                            >
                            @error('days')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputDays" class="col-sm-2 col-form-label">Jam</label>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-6">
                                    <input
                                        class="form-control @error('time_start') is-invalid @enderror"
                                        type="text"
                                        name="time_start"
                                        placeholder="Dari"
                                        value="{{ $course->time_start ?? old('time_start') }}"
                                    >
                                    @error('time_start')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <input
                                        class="form-control @error('time_end') is-invalid @enderror"
                                        type="text"
                                        name="time_end"
                                        placeholder="Sampai"
                                        value="{{ $course->time_end ?? old('time_end') }}"
                                    >
                                    @error('time_end')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 offset-sm-2 text-right">
                        <button type="submit" class="btn btn-primary px-5">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
