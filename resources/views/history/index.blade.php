@extends('layouts.app')

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@endsection

@section('content')

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Laporan</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="{{ request()->url() }}" >
                @csrf

                <div class="mb-4">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Periode</label>
                        <div class="col-sm-4">
                            @php 
                                $daterange = request()->has('daterange') ? request('daterange') : '';
                                $start_date = request()->has('start_date') ? request('start_date') : '';
                                $end_date = request()->has('end_date') ? request('end_date') : '';
                            @endphp
                            <input type="text" id="daterange" class="form-control" name="daterange" placeholder="Pilih Periode" value="{{$daterange}}" autocomplete="off"/>
                            <input type="text" id="start_date" class="form-control" name="start_date" placeholder="Start Date" value="{{$start_date}}" style="display: none;" />
                            <input type="text" id="end_date" class="form-control" name="end_date" placeholder="End Date" value="{{$end_date}}" style="display: none;" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama Dosen</label>
                        <div class="col-sm-4">
                            <select class="form-control js-select-dosen" name="dosen" >
                                <option value="0">Pilih Dosen</option>
                                @foreach(\App\Dosen::all() as $dosen)
                                    @php 
                                        $dosen_checked = '';
                                        if(request()->has('dosen')){
                                            $dosen_checked = request('dosen') == $dosen->id ? 'selected=selected' : '';
                                        } 
                                    @endphp

                                    <option value="{{ $dosen->id }}" {{$dosen_checked}}>{{ $dosen->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Matakuliah</label>
                        <div class="col-sm-4">
                            <select class="form-control js-select-matkul" name="course" >
                                <option value="0">Pilih Mata Kuliah</option>
                                @foreach(\App\Course::all() as $course)
                                    @php 
                                        $course_checked = '';


                                        if(request()->has('course')){
                                            $course_checked = request('course') == $course->id ? 'selected=selected' : '';
                                        } 
                                    @endphp
                                    <option value="{{ $course->id }}" {{$course_checked}}>{{ $course->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 offset-sm-2 text-right">
                        @if(request()->has('course'))
                            <a href="{{ request()->url() }}" class="btn btn-secondary mr-3">Reset</a>
                        @endif
                        <button type="submit" class="btn btn-primary px-5">Filter</button>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-6"></div>
                <div class="col-3 ml-auto">
                    <div class="d-flex align-items-center justify-content-end mb-3">
                        @if(request()->has('search'))
                            <a href="{{ request()->url() }}" class="btn btn-secondary mr-3">Reset</a>
                        @endif

                        <form action="{{ request()->url() }}" style="display: none;">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search by Mata kuliah" aria-label="Search..." aria-describedby="search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit" id="search">
                                        <i class="fas fa-fw fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Periode</th>
                            <th>Jadwal</th>
                            <th>Nama Dosen</th>
                            <th>Mata Kuliah</th>
                            <th>Status Masuk</th>
                            <th>Status Tidak Masuk</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($lectures as $lecture)
                        @php
                            $course = optional($lecture->course);
                            $dosen = optional($course->dosen);
                        @endphp
                        <tr>
                            <td>
                                <span class="text-nowrap">{{ $periode }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $lecture->date->format('l').', '.$lecture->time }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $dosen->name }}</span>
                            </td>
                            <td>
                                <span class="text-nowrap">{{ $course->name }}</span>
                            </td>
                            <td style="text-align: center;">
                                @if($lecture->masuk == 0)
                                <div class="badge badge-success btn-circle btn-sm">{{$lecture->masuk}}</div>
                                @else
                                    <div class="dropdown no-arrow mb-4">
                                        @php
                                            $wheres = [
                                                ['status', '=', '1'],
                                                ['id_course', '=', $lecture->id_course],
                                            ];
                                            $histories = \App\LectureHistory::select('*')->where($wheres);

                                            if(request()->has('start_date') && request()->has('end_date')){
                                                $histories = $histories->whereBetween('date', [request('start_date'), request('end_date')]);
                                            }

                                            $histories = $histories->orderBy('date', 'ASC')->get()->toArray();
                                        @endphp
                                        <button class="badge badge-success btn-circle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          {{$lecture->tidak_masuk}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @foreach($histories as $key => $history)
                                                <a class="dropdown-item">{{date('d F Y', strtotime($history['date']))}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </td>

                            <td style="text-align: center;">
                                @if($lecture->tidak_masuk == 0)
                                <div class="badge badge-danger btn-circle btn-sm">{{$lecture->tidak_masuk}}</div>
                                @else
                                    <div class="dropdown no-arrow mb-4">
                                        @php
                                            $wheres = [
                                                ['status', '=', '2'],
                                                ['id_course', '=', $lecture->id_course],
                                            ];
                                            $histories = \App\LectureHistory::select('*')->where($wheres);

                                            if(request()->has('start_date') && request()->has('end_date')){
                                                $histories = $histories->whereBetween('date', [request('start_date'), request('end_date')]);
                                            }

                                            $histories = $histories->orderBy('date', 'ASC')->get()->toArray();
                                        @endphp
                                        <button class="badge badge-danger btn-circle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          {{$lecture->tidak_masuk}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @foreach($histories as $key => $history)
                                                <a class="dropdown-item">{{date('d F Y', strtotime($history['date']))}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </td>
                           
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            {!! $lectures->links() !!}
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $('.js-select-dosen').select2({
            placeholder: "Pilih Dosen"
        });

        $('.js-select-matkul').select2({
            placeholder: "Pilih Matakuliah"
        });

        $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,
            opens: 'left',
            locale: {
              format: 'DD/MM/YYYY'
            }
          }, function(start, end, label) {
            $('#daterange').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            $('#start_date').val(start.format('YYYY-MM-DD'));
            $('#end_date').val(end.format('YYYY-MM-DD'));
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
          });
    });

</script>
@endsection
