<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    Route::get('/', 'DashboardController');

    Route::get('/schedule', 'ScheduleController@index');
    Route::get('/schedule-screen', 'ScheduleController@screen');
    Route::get('/kirim-konfirmasi', 'ScheduleController@sendConfirmation');
    Route::post('/schedule/{id}/update-status', 'ScheduleController@updateStatus');
    Route::get('/history', 'HistoryController@index');

    // Master Data
    Route::resource('/teacher', 'TeacherController');
    Route::resource('/student', 'StudentController');
    Route::resource('/course', 'CourseController');
});

Route::get('/set-webhook', 'WebhookController@set');
Route::post('/'.config('telegram.bot_token').'/webhook', 'WebhookController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
