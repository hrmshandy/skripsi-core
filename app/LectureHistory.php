<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LectureHistory extends Model
{
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    public function course(){
        return $this->belongsTo(Course::class, 'id_course');
    }

    //
}
