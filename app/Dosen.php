<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $guarded = [];

    /**
     * @param $value
     */
    public function setNoTeleAttribute($value)
    {
        $this->attributes['no_tele'] = '62'.$value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getNoTeleAttribute($value)
    {
        return ltrim($value, "62");
    }
}
