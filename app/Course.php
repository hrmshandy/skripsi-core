<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    public function dosen() {
        return $this->belongsTo(Dosen::class, 'id_dosen', 'id');
    }

    /**
     * The roles that belong to the user.
     */
    public function students()
    {
        return $this->belongsToMany(Student::class);
    }

}
