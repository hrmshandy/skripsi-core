<?php

namespace App\Console\Commands;

use App\Course;
use App\Lecture;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Telegram\Bot\Laravel\Facades\Telegram;

class Lectures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lectures:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Lecture to queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = __('days.'.Str::lower(now()->format('l')));

        $courses = Course::with('dosen')
            ->where('days', 'like', '%'.$today.'%')
            ->whereRaw('time_start >= DATE_SUB(NOW(),INTERVAL 1 HOUR)')
            ->get();

        foreach ($courses as $course) {
            $lecture = Lecture::firstOrCreate([
                'date' => today()->format('Y-m-d'),
                'time' => $course->time_start,
                'id_course' => $course->id
            ]);

            if ($lecture && !$lecture->send) {
                $dosen = optional($course->dosen);

                $keyboard = [
                    [
                        '/ya', '/tidak'
                    ]
                ];

                $replyMarkup = Telegram::replyKeyboardMarkup([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                    'selective' => true
                ]);

                Telegram::sendMessage([
                    'chat_id' => $dosen->chat_id,
                    'text' => 'Yth. Bpk/Ibu '.$dosen->name.', matakuliah '.$course->name.', jam '.$lecture->time.' apakah bisa mengajar?',
                    'reply_markup' => $replyMarkup
                ]);

                Cache::put('lecture-'.$dosen->chat_id, $lecture->id, now()->addMinutes(60));

                $lecture->update(['send' => 1]);
            }
        }
    }
}
