<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];

    /**
     * The roles that belong to the user.
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * @param $value
     */
    public function setNoTeleAttribute($value)
    {
        $this->attributes['no_tele'] = '62'.$value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getNoTeleAttribute($value)
    {
        return ltrim($value, "62");
    }
}
