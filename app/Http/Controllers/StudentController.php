<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::latest();

        if (request()->has('search') && !empty(request('search'))) {
            $students = $students->where('name', 'like', '%'.request('search').'%');
        }

        $students = $students->paginate(10);

        return view('student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'no_tele' => 'required',
            'courses' => 'required'
        ]);

        $data = collect($validated)->except('courses')->all();

        $student = Student::create($data);

        $student->courses()->attach($validated['courses']);

        return redirect('/student')->with('status', 'Mahasiswa has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        return view('student.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'no_tele' => 'required',
            'courses' => 'required'
        ]);

        $data = collect($validated)->except('courses')->all();

        $student = Student::find($id);
        $student->update($data);
        $student->courses()->sync($validated['courses']);

        return redirect('/student')->with('status', 'Mahasiswa has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);

        $student->delete();

        return back()->with('status', 'Mahasiswa has been deleted!');
    }
}
