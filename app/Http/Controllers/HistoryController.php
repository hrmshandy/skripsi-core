<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\LectureHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Telegram\Bot\Laravel\Facades\Telegram;

class HistoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $start_date = request()->has('start_date') ? request('start_date') : date('Y-m-01');
        $end_date   = request()->has('end_date') ? request('end_date') : date('Y-m-t');

        $masuk       = \DB::raw('(SELECT COUNT(*) FROM lecture_histories WHERE `status` = 1 AND lecture_histories.id_course = lectures.id_course AND lecture_histories.date BETWEEN "'.$start_date.'" AND "'.$end_date.'") as masuk');
        $tidak_masuk = \DB::raw('(SELECT COUNT(*) FROM lecture_histories WHERE `status` = 2 AND lecture_histories.id_course = lectures.id_course AND lecture_histories.date BETWEEN "'.$start_date.'" AND "'.$end_date.'") as tidak_masuk');
        $lectures = Lecture::select('*', $masuk, $tidak_masuk)->groupBy('lectures.id_course')->havingRaw('`masuk` != 0 OR `tidak_masuk` != 0')->with('course', 'course.dosen')->latest();

        if (request()->has('search') && !empty(request('search'))) {
            $lectures = $lectures->whereHas('course', function ($query) {
                $query->where('name', 'like', '%'.request('search').'%');
            });
        }

        if (request()->has('course') && !empty(request('course'))) {
            $lectures->where('id_course', request('course'));
        }

        if (request()->has('dosen') && !empty(request('dosen'))) {
            $lectures = $lectures->whereHas('course', function ($query) {
                $query->where('id_dosen', '=', request('dosen'));
            });
        }


        $periode = date('d/m/Y', strtotime($start_date)).' - '.date('d/m/Y', strtotime($end_date));

        // dd($lectures->toSql());

        $lectures = $lectures->simplePaginate(10);

        return view('history.index', compact('lectures', 'periode'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __index()
    {
        $masuk       = \DB::raw('(SELECT COUNT(*) FROM lecture_histories WHERE `status` = 1 AND lecture_histories.id_course = lectures.id_course) as masuk');
        $tidak_masuk = \DB::raw('(SELECT COUNT(*) FROM lecture_histories WHERE `status` = 2 AND lecture_histories.id_course = lectures.id_course) as tidak_masuk');
        $lectures    = LectureHistory::select('*', $masuk, $tidak_masuk)->groupBy('lecture_histories.id_course')->with('course', 'course.dosen')->latest();

        if (request()->has('search') && !empty(request('search'))) {
            $lectures = $lectures->whereHas('course', function ($query) {
                $query->where('name', 'like', '%'.request('search').'%');
            });
        }

        if (request()->has('course') && !empty(request('course'))) {
            $lectures->where('id_course', request('course'));
        }

        if (request()->has('dosen') && !empty(request('dosen'))) {
            $lectures = $lectures->whereHas('course', function ($query) {
                $query->where('id_dosen', '=', request('dosen'));
            });
        }

        $start_date = request()->has('start_date') ? request('start_date') : date('Y-m-01');
        $end_date   = request()->has('end_date') ? request('end_date') : date('Y-m-t');


        $lectures = $lectures->whereBetween('date', [$start_date, $end_date]);

        $periode = date('d/m/Y', strtotime($start_date)).' - '.date('d/m/Y', strtotime($end_date));

        // dd($lectures->toSql());

        $lectures = $lectures->paginate(10);

        return view('history.index', compact('lectures', 'periode'));
    }

}
