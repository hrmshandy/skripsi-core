<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\LectureHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Telegram\Bot\Laravel\Facades\Telegram;

class ScheduleController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lectures = Lecture::with('course', 'course.dosen')->latest();

        $lectures = $lectures->where('validation', '=', 0);

        if (request()->has('search') && !empty(request('search'))) {
            $lectures = $lectures->whereHas('course', function ($query) {
                $query->where('name', 'like', '%'.request('search').'%');
            });
        }

        $lectures = $lectures->paginate(10);

        return view('schedule.index', compact('lectures'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function screen()
    {
        $lectures = Lecture::with('course', 'course.dosen');

        $lectures = $lectures->where('date', '=', date('Y-m-d'))->orderBy('lectures.time', 'ASC')->get();

        return view('schedule.screen', compact('lectures'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request, $id)
    {
        $data = $request->validate([
            'status' => 'required'
        ]);

        $lecture = Lecture::find($id);

        $data['validation'] = 1;

        $update = $lecture->update($data);

        if ($update) {
            $data_history = [
                'date' => $lecture->date,
                'time' => $lecture->time,
                'id_course' => $lecture->id_course,
                'status' => $data['status'] == 3 ? 1 : 2,
                'created_at' => date('Y-m-d H:i:s')
            ];

            LectureHistory::insert($data_history);
        }

        return back()->with('status', 'Lecture status updated');
    }

    /**
     * @return string
     */
    public function sendConfirmation()
    {
        $lectures = Lecture::with('course', 'course.dosen')
            ->where('date', now()->format('Y-m-d'))
            ->where('time', '>=', now()->addHour()->format('H:i:s'))
            ->get();

        // $lectures = Lecture::with('course', 'course.dosen')
        //     ->where('date', now()->format('Y-m-d'))
        //     ->where('time', '>=', now()->addMinutes(30)->format('H:i:s'))
        //     ->get();

        foreach($lectures as $lecture) {

            $course = optional($lecture->course);
            $dosen = optional($course->dosen);

            $keyboard = [
                [
                    '/ya', '/tidak'
                ]
            ];

            $replyMarkup = Telegram::replyKeyboardMarkup([
                'keyboard' => $keyboard,
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'selective' => true
            ]);

            if (!empty($dosen->chat_id)) {
                Telegram::sendMessage([
                    'chat_id' => $dosen->chat_id,
                    'text' => 'Yth. Bpk/Ibu '.$dosen->name.', matakuliah '.$course->name.', jam '.$lecture->time.' apakah bisa mengajar?',
                    'reply_markup' => $replyMarkup
                ]);

                Cache::put('lecture-'.$dosen->chat_id, $lecture->id, now()->addMinutes(60));
            }

        }

        return 'ok';
    }
}
