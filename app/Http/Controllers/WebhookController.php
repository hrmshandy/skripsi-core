<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\Student;
use App\Lecture;
use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Cache;

class WebhookController extends Controller
{
    /**
     * set webhook url
     */
    public function set()
    {
        $response = Telegram::setWebhook([
            'url' => 'https://scheduler.belum.live/'.config('telegram.bot_token').'/webhook',
            // 'certificate' => storage_path('app/public_key_certificate.pub')
        ]);

        dd($response);
    }

    /**
     * @return string
     */
    public function store()
    {
        $updates = Telegram::getWebhookUpdates();
        $message = $updates['message'];

        if (isset($message['text'])) {

            $name = $message['chat']['first_name'] . ' ' . $message['chat']['last_name'];

            if ($message['text'] == '/start') {
                // $this->askTeacherOrStudent($message);
                $this->sendContactRequest($message);

                return 'ok';
            }

            if ($message['text'] == '/ya') {

                $lectureId = Cache::get('lecture-'.$message['chat']['id']);
                $lecture = Lecture::with('course', 'course.dosen', 'course.students')->find($lectureId);

                if ($lecture) {
                    $course = $lecture->course;
                    $dosen = $course->dosen;

                    $lecture->update(['status' => 1]);

                    foreach($course->students as $student) {
                        Telegram::sendMessage([
                            'chat_id' => $student->chat_id,
                            'text' => 'Jangan lupa kepada mahasiswa yang mengambil matakuliah '.$course->name.' dosen ' . $dosen->name . ' pada hari ini masuk pukul '.$lecture->time
                        ]);
                    }
                }

                return 'ok';


            } else if ($message['text'] == '/tidak') {

                $lectureId = Cache::get('lecture-'.$message['chat']['id']);
                $lecture = Lecture::with('course', 'course.dosen', 'course.students')->find($lectureId);

                if ($lecture) {
                    $course = $lecture->course;
                    $dosen = $course->dosen;

                    $lecture->update(['status' => -1]);

                    foreach($course->students as $student) {
                        Telegram::sendMessage([
                            'chat_id' => $student->chat_id,
                            'text' => 'Kepada mahasiswa yang mengambil matakuliah '.$course->name.' dosen ' . $dosen->name . ' pada hari ini tidak ada perkuliahan'
                        ]);
                    }
                }
            }
        }

        if (isset($message['contact'])) {
            $replyMarkup = Telegram::replyKeyboardHide();

            $dosen = Dosen::where('no_tele', $message['contact']['phone_number'])->first();
            $student = Student::where('no_tele', $message['contact']['phone_number'])->first();

            if (!$dosen && !$student) {
                Telegram::sendMessage([
                    'chat_id' => $message['chat']['id'],
                    'text' => 'Mohon maaf nomor handphone Anda tidak terdaftar sebagai mahasiswa ataupun dosen',
                    'reply_markup' => $replyMarkup
                ]);

                return 'ok';
            }

            if ($dosen) {
                $dosen->update([
                    'chat_id' => $message['chat']['id']
                ]);
            }

            if ($student) {
                $student->update([
                    'chat_id' => $message['chat']['id']
                ]);
            }

            Telegram::sendMessage([
                'chat_id' => $message['chat']['id'],
                'text' => 'Terima Kasih',
                'reply_markup' => $replyMarkup
            ]);

            return 'ok';
        }

        return 'ok';
    }

    /**
     * @param $message
     */
    protected function askTeacherOrStudent($message) {
        $keyboard = [
            [
                'Dosen', 'Mahasiswa'
            ]
        ];

        $replyMarkup = Telegram::replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        Telegram::sendMessage([
            'chat_id' => $message['chat']['id'],
            'text' => 'Apakah Anda seorang Dosen atau Mahasiswa ?',
            'reply_markup' => $replyMarkup
        ]);
    }

    /**
     * @param $message
     */
    protected function sendContactRequest($message) {
        $keyboard = [
            [
                [
                    'text' => "Kirim Kontak",
                    'request_contact' => true
                ]
            ]
        ];

        $replyMarkup = Telegram::replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        Telegram::sendMessage([
            'chat_id' => $message['chat']['id'],
            'text' => 'Silahkan kirim kontak Anda untuk melakukan registrasi dengan klik button "Kirim Kontak"',
            'reply_markup' => $replyMarkup
        ]);
    }
}
